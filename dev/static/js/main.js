//MAP
function initMap() {
    var uluru = {lat: 47.81714996173295, lng: 35.16979934647679};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: uluru
    });
    var markerImage = 'http://lms.beetroot.se/images/ba_small.png';
    var markerHome = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: markerImage
    });
}


$(document).ready(function () {

 //SCROLL
    $('.header').on('click', (function(e){
            e.preventDefault();
            var id = e.target.getAttribute('href');
            console.dir(id);
            $('html, body').animate({scrollTop:$(id).position().top}, 500);
        })
    );

//SVG
    svg4everybody({});

//SLIDER
     var $container = $('.portfolio_works').isotope({
        itemSelector: '.portfolio_item',
        layoutMode: 'masonry',
        masonry: {
            horizontalOrder: true,
        }
    });
    $('.portfolio_filter').click(function(){
        var $this = $(this);
        if ( !$this.hasClass('active-filter') ) {
            $this.parents('.portfolio_filters').find('.active-filter').removeClass('active-filter');
            $this.addClass('active-filter');
        }
        var selector = $this.attr('data-filter');
        $container.isotope({  itemSelector: '.portfolio_item', filter: selector });
        return false;
    });

    $('.slider').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        arrows: false,

    });

    $('.slidertestimonials').slick({
        autoplay: true,
        autoplaySpeed: 3000,
        dots: true,
        arrows: false,
    });





});








